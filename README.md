# Gamejam 2021
The best game jam submission

## Setting up
```bash
git clone --depth=1 https://gitlab.com/TestingPlant/gamejam-2021.git
cd gamejam-2021
npm install
rbxtsc
```