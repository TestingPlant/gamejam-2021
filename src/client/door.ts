import { Workspace } from "@rbxts/services";

export class Door {
	private doorPart: BasePart;
	private model: Model;

	public activated = false;

	private timesMoved = 0;
	private closedCFrame: CFrame;
	private doorOpenSound: Sound;

	constructor(position: CFrame, size: Vector3 = new Vector3(0.5, 15, 6), doorOpenSound: Sound, color?: Color3) {
		this.closedCFrame = position;
		this.doorOpenSound = doorOpenSound;

		this.model = new Instance("Model");
		this.model.Name = "Door";

		this.doorPart = new Instance("Part");
		this.doorPart.Size = size;
		this.doorPart.CFrame = this.closedCFrame;
		this.doorPart.Anchored = true;
		this.doorPart.Material = Enum.Material.WoodPlanks;
		color && (this.doorPart.Color = color);
		this.doorPart.Parent = this.model;

		this.model.Parent = Workspace;
	}

	// TweenService was too unreliable so we need to tween it ourselves
	private open() {
		coroutine.wrap(() => {
			let elapsedTime = 0;
			const moveNumber = ++this.timesMoved;
			const start = this.doorPart.CFrame;
			const goal = this.closedCFrame.mul(new CFrame(0, this.doorPart.Size.Y + 5, 0));
			while (elapsedTime < 1 && this.timesMoved === moveNumber) {
				this.doorPart.CFrame = start.Lerp(goal, elapsedTime);
				elapsedTime += wait()[0];
			}
			if (this.timesMoved === moveNumber) this.doorPart.CFrame = goal;
		})();
	}

	private close() {
		coroutine.wrap(() => {
			let elapsedTime = 0;
			const moveNumber = ++this.timesMoved;
			const start = this.doorPart.CFrame;
			const goal = this.closedCFrame;
			while (elapsedTime < 1 && this.timesMoved === moveNumber) {
				this.doorPart.CFrame = start.Lerp(goal, elapsedTime);
				elapsedTime += wait()[0];
			}
			if (this.timesMoved === moveNumber) this.doorPart.CFrame = goal;
		})();
	}

	public set(state: boolean) {
		this.doorOpenSound.Play();
		if (state) {
			this.open();
		} else {
			this.close();
		}
		this.activated = state;
	}
}
