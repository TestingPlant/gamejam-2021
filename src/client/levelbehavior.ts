import { Builder, mountDefaultBuilder } from "./builder";
import { initializeAllBosses } from "./boss";

export const mountLevel = (index: number) => {
	const builder: Builder = new Builder();
	const level: Instance = game.Workspace.WaitForChild("Buildee").WaitForChild(`Level${index}`);

	initializeAllBosses(builder, level);
	mountDefaultBuilder(builder, level);
};

export const mountAllLevels = () => {
	for (let i = 0; i < 11; i++) mountLevel(i);
};
