import BoxCreator, { Creator, ButtonCreator, DoorCreator, LeverCreator, SpringCreator, TimerCreator } from "./creators";

export class Builder {
	private creators: [key: string, creator: Creator][] = [];
	private handlers: { [key: string]: ((...args: unknown[]) => void)[] } = {};

	constructor() {}

	on(name: string, handler: (...args: unknown[]) => void) {
		print(`Registered handler for event ${name}`);
		if (!this.handlers[name]) this.handlers[name] = [handler];
		else this.handlers[name].push(handler);
	}

	emit(name: string, ...args: unknown[]) {
		print(`Event ${name} was emitted. ${this.handlers[name] ? "Calling its callback" : "No callback associated"}`);

		if (!this.handlers[name]) return;

		for (const callback of this.handlers[name])
			callback(...args);
	}

	register(name: string, creator: Creator) {
		this.creators.push([name, creator]);
	}

	build(model: Instance) {
		print(`Builder -> build -> for ${model.Name}`);

		const newModel: Instance = model.Clone();
		newModel.GetDescendants().forEach((object) => this.addObject(object));
		newModel.Parent = model.Parent;
		model.Destroy();
	}

	addObject(object: Instance) {
		if (!object.IsA("BasePart")) return;

		for (const [key, creator] of this.creators) {
			if (!object.Name.find(`^${key}`)[0]) continue;

			if (creator.build?.(object as BasePart, this) === false) object.Destroy();

			return;
		}
	}
}

export const mountDefaultBuilder = (builder: Builder, mountOn?: Instance) => {
	builder.register("Button", new ButtonCreator());
	builder.register("Lever", new LeverCreator());
	builder.register("Spring", new SpringCreator());
	builder.register("Door", new DoorCreator());
	builder.register("Timer", new TimerCreator());
	builder.register("ClickableBox", new BoxCreator());

	mountOn && builder.build(mountOn);
};
