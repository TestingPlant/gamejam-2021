export class Box {
	public activated = false;
	public boxPart: BasePart;

	private controlledBy?: Model;
	private posEvent?: RBXScriptConnection;

	constructor(boxPart: BasePart, dist = 20) {
		this.boxPart = boxPart;
		this.boxPart.SetAttribute("NonSurface", true);
		const clickDetector = new Instance("ClickDetector");
		clickDetector.MouseClick.Connect((plr: Player) => {
			// Roblox doesnt really like working, so I have to reimplement MaxActivationDistance
			const chr = game.Workspace.FindFirstChild(plr.Name) as Model;
			const rootPart = chr.FindFirstChild("Sphere") as Part;
			const mag = rootPart.Position.sub(boxPart.Position).Magnitude;
			if (mag > dist) return;

			print(this.activated);
			if (!this.activated) {
				this.activate(plr);
			} else {
				this.close();
			}
		});
		clickDetector.Parent = boxPart;
	}

	activate(plr: Player) {
		this.boxPart.Orientation = new Vector3(0, 0, 0);
		this.controlledBy = game.Workspace.FindFirstChild(plr.Name) as Model;
		this.activated = true;

		const rootPart = this.controlledBy.FindFirstChild("Sphere") as Part;

		this.posEvent = rootPart.GetPropertyChangedSignal("Position").Connect(() => {
			this.boxPart.Position = rootPart.Position.add(new Vector3(5, 2, 0));
		});
		this.boxPart.Position = rootPart.Position.add(new Vector3(5, 2, 0));
		this.boxPart.Anchored = true;
		this.boxPart.CanCollide = false;
		this.boxPart.Velocity = new Vector3(0, 0, 0);
	}

	close() {
		this.controlledBy = undefined;
		this.posEvent?.Disconnect();
		this.posEvent = undefined;
		this.boxPart.Anchored = false;
		this.boxPart.CanCollide = true;
		this.boxPart.Velocity = new Vector3(0, 0, 0);
		this.activated = false;
	}
}
