const backgroundMusic = new Instance("Sound");
backgroundMusic.SoundId = `rbxassetid://1689003526`;
backgroundMusic.Volume = 0.2;
backgroundMusic.Looped = true;
backgroundMusic.Parent = game.Workspace;

export function playMusic() {
	backgroundMusic.Play();
};
export function stopMusic() {
	backgroundMusic.Pause();
};
