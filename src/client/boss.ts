// MIT License
//
// Copyright (c) 2021 TestingPlant
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { Workspace, RunService, Debris } from "@rbxts/services";
import { Builder } from "./builder";
import { isInteractable, currentTime } from "./shared";
import { Spring } from "./spring";
import { playMusic, stopMusic } from "./music";

export function initializeBoss0(builder: Builder, level: Instance) {
	const springCreators: BasePart[] = [];
	const springCreatorEnabled: boolean[] = [];
	let springCreatorCount = 0;
	let bossActive = false;
	let bossCanStart = true;

	for (const part of level.GetDescendants()) {
		if (part.Name === "CreatorSpring") {
			springCreators.push(part as BasePart);
			++springCreatorCount;
			print(`Registered spring creator ${part.GetFullName()}`);
		}
	}

	function update() {
		for (const enabled of springCreatorEnabled) {
			if (enabled) {
				bossActive = true;
				return;
			}
		}

		if (!bossActive) return;
		bossActive = false;
		builder.emit("Boss0End", true);

		warn("Good job with the first boss! Harder challenges awaits.");
	}

	builder.on("SpringCreator0Toggle", (active) => { springCreatorEnabled[0] = !!active; update(); } );
	builder.on("SpringCreator1Toggle", (active) => { springCreatorEnabled[1] = !!active; update(); } );
	builder.on("SpringCreator2Toggle", (active) => { springCreatorEnabled[2] = !!active; update(); } );

	builder.on("Boss0Start", () => {
		if (bossActive) return;
		if (!bossCanStart) return;
		bossActive = true;
		bossCanStart = false;

		const music = new Instance("Sound");
		music.SoundId = `rbxassetid://487968536`;
		music.Volume = 0.2;
		music.Looped = true;
		music.Parent = game.Workspace;
		music.Play();
		stopMusic();

		builder.emit("SpringCreator0Toggle", true);
		builder.emit("SpringCreator1Toggle", true);
		builder.emit("SpringCreator2Toggle", true);

		while (bossActive) {
			const springCreator = springCreators[math.random(0, springCreatorCount-1)];
			if (!springCreatorEnabled[springCreator.GetAttribute("Index") as number]) {
				print("Picked disabled spring creator");
				wait(0.25);
				continue;
			}
			const spring = new Spring(springCreator.CFrame, new Vector3(0, 100, 0));
			spring.base.Anchored = false;
			Debris.AddItem(spring.model, 20);

			// Randomly pick another spring creator to enable
			if (math.random(1, 12) === 1) {
				const index = math.random(0, springCreatorCount-1);
				print(`Enabling random spring creator number ${index}`);
				if (!springCreatorEnabled[index]) {
					builder.emit(`SpringCreator${index}Toggle`, true);
				}
			}

			wait(0.25);
		}

		music.Stop();
		music.Destroy();
		playMusic();
	});
}

let boss1Started = false;
function initializeBoss1(builder: Builder, spawnpoint?: Part) {
	builder.on("Boss1Start", () => {
		if (boss1Started) return;
		boss1Started = true;
		assert(spawnpoint);

		game.Workspace.GetChildren().forEach((child) => {
			if (child.Name === "Door") child.Destroy();
		});

		const rigPrefab = game.GetService("ReplicatedStorage").WaitForChild("assets").FindFirstChild("FinalBossRig");
		const bossProps = game.GetService("ReplicatedStorage").WaitForChild("assets").FindFirstChild("Boss1Special");
		const mapStart = game.Workspace.WaitForChild("Common").FindFirstChild("FirstLevelStart") as Part;
		const wall = game.Workspace.WaitForChild("Buildee").FindFirstChild("Level0")?.FindFirstChild("OutdoorsWall") as Part;
		const me = game.Workspace.FindFirstChild(game.GetService("Players").LocalPlayer.Name) as Model;

		assert(rigPrefab);
		assert(wall);
		wall.Destroy();

		const rig: Model = rigPrefab.Clone() as Model;

		assert(bossProps);
		const a = bossProps.FindFirstChild("Baseplate");
		assert(a);
		a.Parent = game.Workspace.FindFirstChild("Common");

		const rigHumanoid = rig.FindFirstChildOfClass("Humanoid");
		assert(rigHumanoid);
		const rootPart = rig.FindFirstChild("HumanoidRootPart") as Part;
		assert(rootPart);
		(me.FindFirstChild("Sphere") as Part).Touched.Connect((part) => {
			if (part.Name === "FirstLevelStart") {
				rig.Destroy();
			}
		});

		rig.GetChildren().forEach((child) => {
			if (child.IsA("BasePart")) {
				child.Touched.Connect((part: BasePart) => {
					if (part.Parent?.Name === "Common") return; // Don't fling common elements
					if (part.Name === mapStart.Name) {
						game.Workspace.GetDescendants().forEach((child) => {
							if (child.IsA("BasePart") && child.Parent?.Name !== "Outdoors") {
								child.Anchored = false;
								child.CanCollide = false;
							}
						});
					}
					part.Anchored = false;
					part.CanCollide = false;
					part.Velocity = new Vector3(-100, 100, 50);
				});
			}
		});

		rig.Parent = game.Workspace;
		rig.MoveTo(spawnpoint.Position);
		while (wait()) {
			rigHumanoid.MoveTo(mapStart.Position);
		}
	});
}

export function initializeAllBosses(builder: Builder, level: Instance) {
	initializeBoss0(builder, level);
	initializeBoss1(builder, level.FindFirstChild("BossSpawn") as Part);
}
