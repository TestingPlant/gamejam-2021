// MIT License
//
// Copyright (c) 2021 TestingPlant
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { Workspace, TweenService, RunService } from "@rbxts/services";
import { isInteractable, currentTime } from "./shared";

export class Spring {
	model: Model;

	base: Part;
	baseAttachment: Attachment;
	surface: Part;
	surfaceAttachment: Attachment;

	spring: SpringConstraint;
	prismatic: PrismaticConstraint;
	weld: WeldConstraint;

	height = 3;
	force: Vector3;

	private canUseIn: number = currentTime();

	constructor(platformCFrame: CFrame, newForce: Vector3) {
		const platformSize: Vector3 = new Vector3(2, 0.5, 2);
		const attachmentCFrame: CFrame = CFrame.Angles(0, 0, math.pi / 2.0);

		this.force = newForce;

		this.model = new Instance("Model");
		this.model.Name = "Spring";

		this.base = new Instance("Part");
		this.base.Size = platformSize;
		this.base.CFrame = platformCFrame;
		this.base.Anchored = true;
		this.base.Name = "Base";
		this.base.Material = Enum.Material.DiamondPlate;
		this.base.Parent = this.model;
		this.baseAttachment = new Instance("Attachment");
		this.baseAttachment.CFrame = attachmentCFrame;
		this.baseAttachment.Parent = this.base;

		this.surface = new Instance("Part");
		this.surface.Size = platformSize;
		this.surface.CFrame = platformCFrame.mul(new CFrame(0, this.height, 0));
		this.surface.Name = "Surface";
		this.surface.Material = Enum.Material.DiamondPlate;
		this.surface.Parent = this.model;
		this.surfaceAttachment = new Instance("Attachment");
		this.surfaceAttachment.CFrame = attachmentCFrame;
		this.surfaceAttachment.Parent = this.surface;

		this.spring = new Instance("SpringConstraint");
		this.spring.Coils = 6;
		this.spring.Visible = true;
		this.spring.Attachment0 = this.baseAttachment;
		this.spring.Attachment1 = this.surfaceAttachment;
		this.spring.Parent = this.model;

		this.prismatic = new Instance("PrismaticConstraint");
		this.prismatic.LimitsEnabled = true;
		this.prismatic.LowerLimit = this.height;
		this.prismatic.UpperLimit = this.height;
		this.prismatic.Attachment0 = this.baseAttachment;
		this.prismatic.Attachment1 = this.surfaceAttachment;
		this.prismatic.Parent = this.model;

		this.weld = new Instance("WeldConstraint");
		this.weld.Part0 = this.surface;
		this.weld.Parent = this.model;

		this.model.Parent = Workspace;

		this.surface.Touched.Connect((part: BasePart) => {
			if (this.canUseIn > currentTime()) return;
			if (!isInteractable(part)) return;

			print("Part bouncing off of spring");

			this.weld.Part1 = part;

			const compressStringTween: Tween = TweenService.Create(
				this.prismatic,
				new TweenInfo(0.4, Enum.EasingStyle.Exponential, Enum.EasingDirection.Out),
				{
					LowerLimit: this.height * 0.25,
					UpperLimit: this.height * 0.25,
				},
			);
			const decompressStringTween: Tween = TweenService.Create(
				this.prismatic,
				new TweenInfo(0.125, Enum.EasingStyle.Back, Enum.EasingDirection.Out),
				{
					LowerLimit: this.height,
					UpperLimit: this.height,
				},
			);

			// Don't allow anything else to bounce on this
			this.canUseIn = math.huge;

			compressStringTween.Play();
			compressStringTween.Completed.Connect(() => {
				decompressStringTween.Play();

				this.weld.Part1 = undefined;

				part.Velocity = this.force;
			});
			decompressStringTween.Completed.Connect(() => {
				// 0.125 second delay to avoid the part getting stuck
				this.canUseIn = currentTime() + 0.125;

				part.Velocity = this.force;
			});
		});
	}
}
