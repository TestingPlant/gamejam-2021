// MIT License
//
// Copyright (c) 2021 TestingPlant
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { Workspace, TweenService } from "@rbxts/services";
import { isInteractable, currentTime } from "./shared";

const offColor: Color3 = new Color3(1, 0, 0);
const onColor: Color3 = new Color3(0, 1, 0);

const leverDebounce: number = 0.5;

export class Button {
	model: Model;
	button: Part;
	frame: Part;
	weld: WeldConstraint;

	activated = false;

	// Roblox is very unreliable and we need to reinvent GetTouchingParts().length
	// When TouchEnded is fired, the part passed might still be in the list provided in GetTouchingParts
	private touchingParts: number = 0; ///< Number of parts holding this button down

	private canPlaySoundIn = currentTime();

	onToggled: (button: Button) => void = () => {};

	constructor(at: CFrame, activatedSound: Sound, onlyInteractWithColor?: Color3) {
		this.model = new Instance("Model");
		this.model.Name = "Button";

		this.button = new Instance("Part");
		this.button.Name = "Button";
		this.button.Material = Enum.Material.Neon;
		this.button.Size = new Vector3(3, 0.5, 3);
		this.button.CFrame = at;
		this.button.Color = offColor;
		this.button.Parent = this.model;

		this.frame = new Instance("Part");
		this.frame.Name = "Frame";
		this.frame.Size = new Vector3(3.5, 0.5, 3.5);
		this.frame.CFrame = at.mul(new CFrame(0, -0.25, 0));
		this.frame.Anchored = true;
		this.frame.Material = Enum.Material.DiamondPlate;
		this.frame.Parent = this.model;
		onlyInteractWithColor && (this.frame.Color = onlyInteractWithColor);

		this.weld = new Instance("WeldConstraint");
		this.weld.Part0 = this.button;
		this.weld.Part1 = this.frame;
		this.weld.Parent = this.model;

		this.model.Parent = Workspace;

		this.button.Touched.Connect((part: BasePart) => {
			if (!isInteractable(part)) return;
			if (onlyInteractWithColor && part.Color !== onlyInteractWithColor) return;
			++this.touchingParts;

			assert(this.touchingParts === 1 || this.activated); // This should be either the only part touching it or already activated
			if (this.activated) return;
			assert(this.touchingParts === 1); // This should be the only part that's activating the button

			this.activated = true;

			this.button.Color = onColor;

			if (currentTime() > this.canPlaySoundIn)
				activatedSound.Play();
			this.canPlaySoundIn = currentTime() + 0.25;

			this.onToggled(this);
		});
		this.button.TouchEnded.Connect((part: BasePart) => {
			if (!isInteractable(part)) return;
			if (onlyInteractWithColor && part.Color !== onlyInteractWithColor) return;
			--this.touchingParts;

			assert(this.touchingParts >= 0, `Interactable part ${part.GetFullName()} stopped touching a button when Button.touchingParts <= 0`);

			assert(this.activated, `Interactable part ${part.GetFullName()} stopped touching a button when the button wasn't activated`);

			if (this.touchingParts > 0) return; // Something else is keeping this down

			this.activated = false;
			this.button.Color = offColor;

			this.onToggled(this);
		});
	}
}

export class Lever {
	model: Model;

	frame: Part;
	lever: Part;
	indicator: Part;

	activated = false;

	private canUseIn = currentTime();
	private activatedSound: Sound;

	onToggled: (lever: Lever) => void = () => {};

	constructor(at: CFrame, activatedSound: Sound) {
		this.activatedSound = activatedSound;
		this.model = new Instance("Model");
		this.model.Name = "Lever";

		this.frame = new Instance("Part");
		this.frame.Anchored = true;
		this.frame.Size = new Vector3(5, 0.5, 3);
		this.frame.CFrame = at;
		this.frame.CanCollide = false;
		this.frame.Material = Enum.Material.DiamondPlate;
		this.frame.Name = "Frame";
		this.frame.Parent = this.model;

		this.indicator = new Instance("Part");
		this.indicator.Anchored = true;
		this.indicator.Size = new Vector3(1, 1, this.frame.Size.Z);
		this.indicator.CFrame = at;
		this.indicator.Material = Enum.Material.Neon;
		this.indicator.CanCollide = false;
		this.indicator.Name = "Indicator";
		this.indicator.Parent = this.model;

		this.lever = new Instance("Part");
		this.lever.Anchored = true;
		this.lever.Size = new Vector3(1, 4, 0.5);
		this.lever.CFrame = at;
		this.lever.Material = Enum.Material.WoodPlanks;
		this.lever.Name = "Lever";
		this.lever.Parent = this.model;

		this.flip(false);

		this.model.Parent = Workspace;

		this.lever.Touched.Connect((part: BasePart) => {
			if (!isInteractable(part)) return;
			if (this.canUseIn > currentTime()) return;

			// Check if they're on the other side of the lever,
			// which would indicate that they're trying to flip the lever
			const dot: number = this.lever.CFrame.UpVector.Dot(part.Position.sub(this.lever.Position).Unit);
			if (dot < 0) return;

			this.flip(!this.activated);
		});
	}

	flip(to: boolean) {
		this.activated = to;

		const length = 0.25;

		const tween = TweenService.Create(this.lever, new TweenInfo(length, Enum.EasingStyle.Linear), {
			CFrame: this.frame.CFrame.mul(CFrame.Angles(0, 0, ((this.activated ? 1 : -1) * math.pi) / 4)).mul(
				new CFrame(0, this.lever.Size.Y / 2.0, 0),
			),
		});
		tween.Play();
		this.activatedSound.Play();
		this.indicator.Color = this.activated ? onColor : offColor;
		this.canUseIn = currentTime() + length + leverDebounce;
		this.onToggled(this);
	}
}
