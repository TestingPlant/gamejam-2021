// MIT License
//
// Copyright (c) 2021 TestingPlant
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { Workspace, TweenService, RunService } from "@rbxts/services";
import { currentTime } from "./shared";

export class Timer {
	model: Model;

	indicator: Part;
	frame: Part;

	onDone: (timer: Timer) => void = ()=>{};

	duration: number;

	private active = false;
	private tween: Tween;

	constructor(at: CFrame, height: number, duration: number) {
		this.duration = duration;

		const platformSize: Vector3 = new Vector3(2, 0.5, 2);
		const attachmentCFrame: CFrame = CFrame.Angles(0, 0, math.pi / 2.0);

		this.model = new Instance("Model");
		this.model.Name = "Timer";

		this.indicator = new Instance("Part");
		this.indicator.CFrame = at.mul(new CFrame(0, 0, 0.25));
		this.indicator.Size = new Vector3(0.5, height, 0.5);
		this.indicator.Color = new Color3(1, 1, 0);
		this.indicator.Material = Enum.Material.Neon;
		this.indicator.Anchored = true;
		this.indicator.Parent = this.model;

		this.frame = new Instance("Part");
		this.frame.CFrame = at;
		this.frame.Size = new Vector3(0.75, height+0.25, 0.75);
		this.indicator.Anchored = true;
		this.frame.Parent = this.model;

		this.model.Parent = Workspace;

		this.tween = TweenService.Create(this.indicator, new TweenInfo(this.duration, Enum.EasingStyle.Linear), {
			Size: this.indicator.Size.mul(new Vector3(1, 0, 1)),
			CFrame: this.indicator.CFrame.mul(new CFrame(0, -this.indicator.Size.Y/2.0, 0))
		});
		this.tween.Completed.Connect(() => this.onDone(this));
	}

	start() {
		if (this.active) {
			warn("Timer was started multiple times. Ignoring Timer.start() call");
			return;
		}
		this.active = true;

		this.tween.Play();
	}
	stop() {
		if (!this.active) {
			warn("Timer was stopped multiple times. Ignoring Timer.stop() call");
			return;
		}
		this.active = false;

		this.tween.Pause();
	}
}
