import { Box } from "./box";
import { Builder } from "./builder";
import { Door } from "./door";
import { Spring } from "./spring";
import { Button, Lever } from "./switches";
import { Timer } from "./timer";

const switchFlippedSoundId = "5745645740";
const doorOpenSoundId = "863544883";

const switchFlippedSound = new Instance("Sound");
switchFlippedSound.SoundId = `rbxassetid://${switchFlippedSoundId}`;
switchFlippedSound.Parent = game.Workspace;
switchFlippedSound.Volume = 0.2;

const doorOpenSound = new Instance("Sound");
doorOpenSound.SoundId = `rbxassetid://${doorOpenSoundId}`;
doorOpenSound.Parent = game.Workspace;
doorOpenSound.Volume = 0.2;

export class Creator {
	build?(part: BasePart, builder: Builder): boolean;
}

export class ButtonCreator extends Creator {
	build(part: BasePart, builder: Builder) {
		const buttonCallbackName = part.GetAttribute("Callback");
		const onlyInteractWithColor = part.GetAttribute("OnlyInteractWith");

		let button: Button;
		if (typeIs(onlyInteractWithColor, "Color3")) {
			button = new Button(part.CFrame, switchFlippedSound, onlyInteractWithColor);
		} else {
			button = new Button(part.CFrame, switchFlippedSound);
		}

		if (typeIs(buttonCallbackName, "string")) {
			button.onToggled = ({ activated }) => builder.emit(buttonCallbackName, activated);
		}

		return false;
	}
}

export class SpringCreator extends Creator {
	build(part: BasePart) {
		const forceString = part.Name.match("%d+")[0];
		assert(forceString, "Spring name should include force");
		const force: number = tonumber(forceString) as number;
		assert(force);

		new Spring(part.CFrame, part.CFrame.UpVector.mul(force));

		return false;
	}
}

export class LeverCreator extends Creator {
	build(part: BasePart, builder: Builder) {
		const lever = new Lever(part.CFrame, switchFlippedSound);

		const leverCallbackName = part.GetAttribute("Callback");
		if (typeIs(leverCallbackName, "string")) {
			let expectedState: boolean = lever.activated;

			lever.onToggled = ({ activated }) => {
				if (expectedState === activated) return;
				expectedState = activated as boolean;
				builder.emit(leverCallbackName, activated);
			};

			// Allow something else to flip this lever
			builder.on(leverCallbackName, (state) => {
				if (lever.activated !== state) {
					expectedState = state as boolean;
					lever.flip(expectedState);
				}
			});
		}

		return false;
	}
}

export class DoorCreator extends Creator {
	build(part: BasePart, builder: Builder) {
		const door = new Door(part.CFrame, part.Size, doorOpenSound, new Color3(0.3, 0, 0));

		const doorTriggerName = part.GetAttribute("Trigger");
		if (typeIs(doorTriggerName, "string")) {
			builder.on(doorTriggerName, (state) => door.set(state as boolean));
		}

		return false;
	}
}

export class TimerCreator extends Creator {
	build(part: BasePart, builder: Builder) {
		let duration = part.GetAttribute("Duration");
		if (!typeIs(duration, "number")) error("Duration attribute is not a number on the timer object");

		const timer = new Timer(part.CFrame, part.Size.Y, duration as number);

		const timerTriggerName = part.GetAttribute("Trigger");
		if (typeIs(timerTriggerName, "string")) {
			builder.on(timerTriggerName, (state) => state ? timer.start() : timer.stop());
		}
		const timerCallbackName = part.GetAttribute("Callback");
		if (typeIs(timerCallbackName, "string")) {
			timer.onDone = () => builder.emit(timerCallbackName);
		}

		return false;
	}
}

export default class BoxCreator extends Creator {
	build(part: BasePart, builder: Builder) {
		const box = new Box(part);
		return true;
	}
}
