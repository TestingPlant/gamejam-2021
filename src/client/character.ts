// MIT License
//
// Copyright (c) 2021 TestingPlant
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { Workspace, Players, RunService, ContextActionService } from "@rbxts/services";
import { setInteractable, currentTime } from "./shared";

const player: Player = Players.LocalPlayer;

function absoluteMax(a: number, b: number): number {
	if (math.abs(a) > math.abs(b)) return a;
	else return b;
}

export class Character {
	model: Model;

	sphere: Part;
	light: PointLight;

	touchedThreshold = 0.25; ///< Current time - lastTouchedSurface must be less than this value to jump
	lastTouchedSurface: number;

	rollForce = 15;
	jumpForce = 60;

	constructor() {
		const player: Player = Players.LocalPlayer;

		this.model = new Instance("Model");

		this.sphere = new Instance("Part");
		this.sphere.Shape = Enum.PartType.Ball;
		this.sphere.CustomPhysicalProperties = new PhysicalProperties(0.2, 0.7, 0, 0.8, 0);
		setInteractable(this.sphere, true);
		this.sphere.Name = "Sphere";
		this.sphere.Material = Enum.Material.Neon;
		this.sphere.Color = new Color3(0, 1, 0);
		this.sphere.Parent = this.model;
		this.model.PrimaryPart = this.sphere;
		this.model.Name = player.Name;

		this.light = new Instance("PointLight");
		this.light.Range = 15;
		this.light.Color = this.sphere.Color;
		this.light.Parent = this.sphere;

		this.model.Parent = Workspace;

		if (player.Character) player.Character.Destroy();
		player.Character = this.model;

		// The physics engine runs at 240hz if I remember correctly, so we need a touched event to detect
		// when the player touched the ground for a short amount of time
		this.sphere.Touched.Connect((part: BasePart) => {
			if (part.Name === "Wall") return;
			if (part.GetAttribute("NonSurface")) return;
			this.lastTouchedSurface = currentTime();
		});

		// We also want to update lastTouchedSurface when the player is staying on a part, so
		// we need this heartbeat connection
		const connection: RBXScriptConnection = RunService.Heartbeat.Connect(() => {
			if (!this.sphere.Parent) {
				print("Character has been destroyed, removing heartbeat connection");
				connection.Disconnect();
				return;
			}
			for (const part of this.sphere.GetTouchingParts()) {
				if (part.Name === "Wall") continue;
				if (part.GetAttribute("NonSurface")) continue;
				this.lastTouchedSurface = currentTime();
				break;
			}
		});

		// Don't allow them to jump until they've touched the ground
		// Maybe they've spawned in teh air or something
		this.lastTouchedSurface = currentTime() - this.touchedThreshold;

		// Set up movement
		const holding: { [key: number]: boolean } = {};
		RunService.RenderStepped.Connect(() => {
			const isTouchingGround: boolean = currentTime() - this.lastTouchedSurface < this.touchedThreshold;
			let moveVector: Vector3 = new Vector3();

			if (holding[Enum.KeyCode.A.Value]) moveVector = moveVector.add(new Vector3(-1, 0, 0));
			if (holding[Enum.KeyCode.D.Value]) moveVector = moveVector.add(new Vector3(1, 0, 0));
			if (holding[Enum.KeyCode.Space.Value] && isTouchingGround) {
				this.sphere.Velocity = this.sphere.Velocity.add(new Vector3(0, this.jumpForce, 0));

				// Don't allow the player to double jump
				this.lastTouchedSurface -= this.touchedThreshold;
				holding[Enum.KeyCode.Space.Value] = false;
			}

			moveVector = moveVector.mul(this.rollForce);

			this.sphere.Velocity = isTouchingGround
				? this.sphere.Velocity.Lerp(new Vector3(moveVector.X, this.sphere.Velocity.Y, moveVector.Z), 0.2)
				: new Vector3(
						absoluteMax(this.sphere.Velocity.X, moveVector.X),
						absoluteMax(this.sphere.Velocity.Y, moveVector.Y),
						absoluteMax(this.sphere.Velocity.Z, moveVector.Z),
				  );

			// Prevent player from being knocked into the 3rd dimension across the Z axis
			this.sphere.Position = this.sphere.Position.mul(new Vector3(1, 1, 0));

			assert(Workspace.CurrentCamera, "Workspace.CurrentCamera should not be undefined");
			Workspace.CurrentCamera.CFrame = new CFrame(this.sphere.Position).mul(new CFrame(0, 0, 20));
		});

		const onMove = (_: string, state: Enum.UserInputState, input: InputObject) => {
			holding[input.KeyCode.Value] = state === Enum.UserInputState.Begin;
		};
		ContextActionService.BindAction("move", onMove, false, Enum.KeyCode.A, Enum.KeyCode.D, Enum.KeyCode.Space);

		print("Created character");
	}
}
